using Core.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public class StoreContextSeed
    {
        public static async Task SeedAsync(StoreContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                if (!context.ProductBrands.Any())
                {
                    context.ProductBrands.AddRange(
                        new ProductBrand
                        {
                            Name = "Angular"
                        },

                        new ProductBrand
                        {
                            Name = "NetCore"
                        },

                        new ProductBrand
                        {
                            Name = "VS Code"
                        },

                        new ProductBrand
                        {
                            Name = "React"
                        },

                        new ProductBrand
                        {
                            Name = "Typescript"
                        },

                        new ProductBrand
                        {
                            Name = "Redis"
                        }
                    );

                    await context.SaveChangesAsync();
                }

                if (!context.ProductTypes.Any())
                {
                    context.ProductTypes.AddRange(
                        new ProductType
                        {
                            Name = "Boards"
                        },
                        new ProductType
                        {
                            Name = "Hats"
                        },
                        new ProductType
                        {
                            Name = "Boots"
                        },
                        new ProductType
                        {
                            Name = "Gloves"
                        }
                    );

                    await context.SaveChangesAsync();
                }

                if (!context.Products.Any())
                {
                    context.Products.AddRange(
                        new Product
                        {
                            Name = "Green Angular Board 3000",
                            Description = "Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.",
                            Price = 150,
                            PictureUrl = "images/products/sb-ang2.png",
                            ProductTypeId = 1,
                            ProductBrandId = 1
                        },
                        new Product
                        {
                            Name = "Angular Speedster Board 2000",
                            Description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.",
                            Price = 200,
                            PictureUrl = "images/products/sb-ang1.png",
                            ProductTypeId = 1,
                            ProductBrandId = 1
                        },
                        new Product
                        {
                            Name = "Core Board Speed Rush 3",
                            Description = "Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.",
                            Price = 180,
                            PictureUrl = "images/products/sb-core1.png",
                            ProductTypeId = 1,
                            ProductBrandId = 2
                        },
                        new Product
                        {
                            Name = "Net Core Super Board",
                            Description = "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.",
                            Price = 300,
                            PictureUrl = "images/products/sb-core2.png",
                            ProductTypeId = 1,
                            ProductBrandId = 2
                        },
                        new Product
                        {
                            Name = "React Board Super Whizzy Fast",
                            Description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.",
                            Price = 250,
                            PictureUrl = "images/products/sb-react1.png",
                            ProductTypeId = 1,
                            ProductBrandId = 4
                        },
                        new Product
                        {
                            Name = "Typescript Entry Board",
                            Description = "Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.",
                            Price = 120,
                            PictureUrl = "images/products/sb-ts1.png",
                            ProductTypeId = 1,
                            ProductBrandId = 5
                        },
                        new Product
                        {
                            Name = "Core Blue Hat",
                            Description = "Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.",
                            Price = 10,
                            PictureUrl = "images/products/hat-core1.png",
                            ProductTypeId = 2,
                            ProductBrandId = 2
                        },
                        new Product
                        {
                            Name = "Green React Woolen Hat",
                            Description = "Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.",
                            Price = 8,
                            PictureUrl = "images/products/hat-react1.png",
                            ProductTypeId = 2,
                            ProductBrandId = 4
                        },
                        new Product
                        {
                            Name = "Purple React Woolen Hat",
                            Description = "Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.",
                            Price = 15,
                            PictureUrl = "images/products/hat-react2.png",
                            ProductTypeId = 2,
                            ProductBrandId = 4
                        },
                        new Product
                        {
                            Name = "Blue Code Gloves",
                            Description = "Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus.",
                            Price = 18,
                            PictureUrl = "images/products/glove-code1.png",
                            ProductTypeId = 4,
                            ProductBrandId = 3
                        },
                        new Product
                        {
                            Name = "Green Code Gloves",
                            Description = "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.",
                            Price = 15,
                            PictureUrl = "images/products/glove-code2.png",
                            ProductTypeId = 4,
                            ProductBrandId = 3
                        },
                        new Product
                        {
                            Name = "Purple React Gloves",
                            Description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa.",
                            Price = 16,
                            PictureUrl = "images/products/glove-react1.png",
                            ProductTypeId = 4,
                            ProductBrandId = 4
                        },
                        new Product
                        {
                            Name = "Green React Gloves",
                            Description = "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.",
                            Price = 14,
                            PictureUrl = "images/products/glove-react2.png",
                            ProductTypeId = 4,
                            ProductBrandId = 4
                        },
                        new Product
                        {
                            Name = "Redis Red Boots",
                            Description = "Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.",
                            Price = 250,
                            PictureUrl = "images/products/boot-redis1.png",
                            ProductTypeId = 3,
                            ProductBrandId = 6
                        },
                        new Product
                        {
                            Name = "Core Red Boots",
                            Description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna.",
                            Price = 189.99M,
                            PictureUrl = "images/products/boot-core2.png",
                            ProductTypeId = 3,
                            ProductBrandId = 2
                        },
                        new Product
                        {
                            Name = "Core Purple Boots",
                            Description = "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci.",
                            Price = 199.99M,
                            PictureUrl = "images/products/boot-core1.png",
                            ProductTypeId = 3,
                            ProductBrandId = 2
                        },
                        new Product
                        {
                            Name = "Angular Purple Boots",
                            Description = "Aenean nec lorem. In porttitor. Donec laoreet nonummy augue.",
                            Price = 150,
                            PictureUrl = "images/products/boot-ang2.png",
                            ProductTypeId = 3,
                            ProductBrandId = 1
                        },
                        new Product
                        {
                            Name = "Angular Blue Boots",
                            Description = "Suspendisse dui purus, scelerisque at, vulputate vitae, pretium mattis, nunc. Mauris eget neque at sem venenatis eleifend. Ut nonummy.",
                            Price = 180,
                            PictureUrl = "images/products/boot-ang1.png",
                            ProductTypeId = 3,
                            ProductBrandId = 1
                        }
                    );

                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                var logger = loggerFactory.CreateLogger<StoreContextSeed>();
                logger.LogError(ex.Message);
            }
        }
    }
}